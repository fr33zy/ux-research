# Survey about requirements for monitoring – Survey

## Intro 
Hello from GitLab,

when building our product, we strive to make decisions that are informed by our users.
 
Goal of this survey is collect data about one changes that we plan to do to one of our features so we can prioritize them properly and work on the ones that are the most desired.

## Screening questions

:warning: Please carefully read all the questions before answering them.

What is your current job title?
- Open answer

 Which of the following tasks are you responsible for on a regular basis?
* Developing application software – Yes/No
* Performing unit testing – Yes/No
* Administering CI/CD – Yes/No
* Monitoring and analyzing application performance – Yes/No <!--Required-->
* Triaging incidents and performing root cause analysis – Yes/No
* Managing infrastructure – Yes/No
* I'm not responsible for any of these tasks – Yes/No

## Current features
*Described in the `Features list.md`*

## Let's get started
Now you will be presented with a set of new features that we plan to implement.
 
Please, evaluate these features as if you were allowed to use only GitLab and no other tool to analyze your application performance.

## Kano features
- Description *Described in the `Features list.md`*
- Image *Described in the `Features list.md`*
- Functional question: How would you feel if you had this feature?
     - I am delighted by it
     - I expect it
     - I am neutral
     - I can tolerate it
     - I am frustrated by it
- Disfunctional question: How would you feel if you did not have this feature?
     - I am delighted by it
     - I expect it
     - I am neutral
     - I can tolerate it
     - I am frustrated by it
     
## Additional questions

A few last questions before we finish

How many dashboards you have configured?
- 1-10
- 10-50
- 50-100
- 100+

How do you configure your dashboards most of the time?
- Manually by hand
- Automatically using scripts
- We use out-of-the-box dashboards

How many users in your organization have access to those dashboards?
- Open answer

How many people are part of your team?
- Open answer

From the list bellow select all types of visualization you currently use for your metrics chart?
- Line chart
- Bar chart
- Gauge
- Table view
- Markdowns
- Single stat
- Log scale
- Other_________

What is the presented timezone on your metric chart?
- Local timezone
- UTC
- other

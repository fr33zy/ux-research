# Metrics (Dashboards) – Features list

Descriptions of features that we will present to the respondents. Guidelines for writing feature description can be found in [Feature description guidelines](/Feature description guidelines.md).

## JTBD
When analyzing application performance, I can see the important health and performance indicators for my application, so that I am informed when taking the next steps. ([Source](https://gitlab.com/gitlab-org/gitlab/-/issues/216479#note_365899872))

## Current features

Before we jump to the rest of our questions, please familiarize yourself with how does the feature look now:
 
Dashboards are one of the monitoring features offered currently in GitLab. Configuration of a dashboard is done by editing the dashboard's yaml file. Dashboard consists of several panels. Each panel can contain a chart, a numerical indicator or a static markdown text. When users create an alert for some of the metrics that are displayed on a dashboard, it is visualized on the metric chart.
 
Bellow you can see a screenshot of how does an example dashboard and configuration YAML file currently look in GitLab. 

<img src='./image_9.png' width=600px />
<img src='./image_10.png' width=600px />

## New features

### Dashboard level

##### ⭐Dashboard templates

Templates for YAML configuration files
that you can choose when creating new dashboards. 
Templates contain basic file structure, configuration examples, and comments
to speed up the dashboard creation process and improve consistency across dashboards.

##### ⭐Variables

Instead of the time-consuming and repetitive task of recreating similar dashboards multiple times, you can create a single dashboard and use variables to change the data you view in it.

<img src='./image_7.png' width=600px />

##### ⭐OOTB dashboards

A set of out-of-the-box dashboards for the most common technologies and services
that are configured automatically when you set up monitoring in GitLab. 
It will improve the experience of users that just started using the monitoring 
features of GitLab. 

##### ⭐Folders

You can add dashboards to folders to make the dashboards list more organized.

##### ⭐Tags

You can tag dashboards with keywords to make the searching for dashboards faster. 

##### ⭐Dashboard hyperlinks list

A special horizontal list of custom hyperlinks
so you can drill down to a tool of your choice (e.g. another dashboard or external tool) right from the dashboard UI 
what can speed up switching between different tools.

<img src='./image_11.png' width=600px /> 

##### ⭐Rearange panels with cursor

You can rearrange panels using drag and drop rather than by editing the YAML file. 

<img src='./image.png' width=600px />

##### ⭐Resize panels with cursor

You can resize panels using cursor rather than by editing the YAML file.

### Panel level

##### ⭐Panel hyperlinks list

Custom hyperlink in the options menu of a panel,
so you can drill down to a tool of your choice (e.g. another dashboard or external tool) right from the relevant panel
that can speed up switching between different tools.

<img src='./image_2.png' width=600px />

##### ⭐Annotations

Annotation, a dashed vertical blue line in a chart that displays a custom message on hover,
that you can add to highlight an important point within the chart.
To improve collaboration and informativeness of the chart.

<img src='./image_3.png' width=600px />

##### ⭐Threshold lines

Threshold line, a solid horizontal red line in a chart,
that you can add to visualize threshold without triggering an alert.

<img src='./image_8.png' width=600px />

##### ⭐Alert config modal

A contextual alert configuration modal openable from within the metric chart
where you can configure alerts without the necessity to navigate to a dedicated alert configuration page.
It will improve the speed of alert configuration and provide better awareness of the underlying metric.

<img src='./image_6.png' width=600px />

##### ⭐Line appearance editing

You can change the appearance of lines on a chart – color, width, type of line (solid, dotted...).

##### ⭐Homepage dashboard by admin

As an admin or a maintainer of a project, you can set a dashboard as the Metrics home page which will be the default landing page for Metrics for all users.

##### ⭐Homepage dashboard by user

As an individual user, you can set the dashboard they use the most as the Metrics home page, which will override the default admin setting.

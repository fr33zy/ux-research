# Interview about triage workflow – Discussion guide
## Session
### General intro
Hi, my name is Andrej, I am UX researcher in GitLab. 
My colleague Dov, a product manager,  will join me today to help with taking 

How are you doing? 

Todays session consists of two parts: First I will ask you some questions about yourself, then we will discuss our main topic – will tell you more later. 

This is not an exam or test. In case some things do not work ideally, don't be worried about sharing it. These are exactly the things that we want to learn more about.

Before we start, I would like to remind you, that we will record this session.
The recording will not be published anywhere publicly. I will use it for the analysis purposes and possibly to communicate some interesting findings to the team.
 

### Initial interview
- What kind of work does your company do?
- What is your job title?
- For how long have you been working in this role?
- What does your typical working day involve?
- What made you become a [job title]?
- What languages/frameworks/tools do you use? Do you use GitLab?

### Topic intro
Our main topic is triaging incidents. I would like to ask you about how do you triage incidents in your work from the very beginning up until they are ready to be fixed.

I would like to learn more about the steps that you take, people who are 
involved and tools that you use. We are curious about it because we are currently working on features connected to triage in GitLab and we want to make it as useful to our users as possible.

### Incident description

- In the screener you replied that you are <responsible for triaging incidents and performing a Root cause analysis>. How many time did you do these activities in
last 7 days? 
- For last 3 incidents can you tell es more about 
  - What kind of incident was it? (500 error, performance, user complain etc.)
  - How serious was it?
  - When did it happen?
  - Were you notified about this incident? If yes, how? (tool) Who else was notified?

Lets focus more on the first/second/third incident you mentioned. (pick critical)

### Workflow
*Always ask about both ACTION and TOOL*

**Steps**
- Step: Incident was noticed by a person
   - What was the first action? What were the following actions?
- Step: Person was assigned to find a root cause
   - Why was this person assigned?
- Step: Root cause analysis was finished and a person was assigned to fix it
   - What kind of dashboard you look into? How many of them were viewed during the RCA? Did you used the OOTB(out of the box) dashboard or was it a custom made one? did you had to create Ad-hoc dashboards?
   - How did you create this dashboard?
   
**Conditional questions**
- In case participant mentions handover to different person:
   - What is the role of this person?
   - Why did this handover happen? How did you do it?
- In case participant mentions switching to a different tool:
   - How did you navigate to this tool/step? (ask about details)
   - How is this tool integrated with the previous one? Do you have to do any
     manual actions (e.g. transfer data or information) that can be automated?
     
**Final questions**
- If participant did not mention these themselves, ask in the end:
  - Did you use logs? What tools did you use to view them?
  - Did you use metrics? What tools did you use to view them?
- Flow of the data:
  - How does the data flow into logs or metrics? When is the data stored? 

### Summary
Sum up following:
- Number of people involved
- Number of tools used

### Incident comparison
- Are there any other incidents that are very different than the one we 
talked about?
   - Number of people involved
   - Number of tools used
- How does your triage workflow differ for these incidents? 
- When you are working on an incident, do you ever search for historical incidents to help fix the current problem?

## Debrief
- Participant specifics
- Incident specifics
- Workflow insights

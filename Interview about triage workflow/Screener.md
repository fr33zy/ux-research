# Interview about triage workflow – Screener

## Sessions
- 6x 1 hour long sessions

## Screener questionnaire 
### Intro
Hello from GitLab,

I would like to learn how you deal with triaging incidents in your organization.

Following is a short questionnaire (~2 minutes to complete) where you can share 
some information about you. From all the respondents, I will select 5 that match
our recruitment criteria.

If you are selected, I will invite you to a 1-hour long video call where I will 
ask you what is your way to triage incidents. Participation in this call 
is rewarded with $120.

If you are interested, please continue to the questionnaire. 

### Questions
<!-- Role -->
Which of the following most closely matches your job title? (feel free to mark more than one if you are responsible for multiple jobs)
- Software Engineer/Developer
- Full-stack Engineer/Developer
- Back-end Engineer/Developer
- Front-end Engineer/Developer
- DevOps Engineer
- Release Manager
- Operations Engineer
- Systems Administrator/Engineer
- Infrastructure Engineer
- Site Reliability Engineer
- Quality Assurance Engineer
- Security Professional
- Development Team Lead
- Executive (VP of Eng., CTO, CIO, etc)
- Product Manager
- Project Manager
- Designer
- Researcher
- Scientist
- Student
- Unemployed
- Other (please specify)

<!-- JTBD -->
Which of the following tasks are you responsible for on a regular basis? (choose all that apply)
- Triage incidents <!-- Important -->
- Perform root cause analysis  <!-- Important -->
- Build CI pipeline
- QA the code
- Run automated tests
- Write and perform unit tests
- Lead the DevOps process
- Perform tracing
- Collect metrics
- Track and resolve errors from code
- Monitor application/ applications running in production
- Decide what is ready to be released and deploy code to pipeline
- I'm not responsible for any of these tasks

### Company 
<!-- Size -->
Approximately, how many people work in your company?
- 0 - 10 people
- 11 - 100 people
- 101 - 500 people
- 501 - 1000 people
- 1001 - 10,000 people
- 10,000+ people

Besides yourself, how many other people work within the engineering team 
of your company?

- 0 - it's just me!
- 1 - 5 people
- 6 - 10 people
- 11 - 20 people
- 21 - 30 people
- 30+ people

<!-- Industry -->
Which industry best describes your company?

- Healthcare
- Finance
- Creative
- Technology
- Government
- Automotive
- Other (text field)

### Permissions
The video call will be recorded. The recording will be used for analysis purposes 
and to communicate findings within the company. The recording will not be 
published to any publicly available palace.

Please confirm bellow that you grant us permission to record during the call:

- I agree with recording
- I disagree with recording

### Contact
Please provide your contact information below. It will only be used to contact 
you in regards to this study. For instance, to schedule your session 
and to contact you should you not show on time.

Name: 

Email:

Phone (with country code): 

### Thank you
Thank you for taking this questionnaire. 

If your responses match with the recruitment criteria, I will contact you. 

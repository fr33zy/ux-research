# User testing of triage workflow – Research plan

## Research questions
##### Triage workflow part 
1. What problems fo users face when triaging incidents?

##### Incident issue part
1. What are the first impressions from the new design?
1. What are users thoughts about the new features?

## Method
##### Triage workflow part
A usability testing where participants will be asked to triage a recent incident in a test project. 

##### Incident issue participant
A pluralistic-walkthrough-like feedback session of a new incident issue designs where participants will be asked for their first impressions and then walked through each of the new features and asked for feedback. 

### Data analysis
Data will be collected and analyzed in Dovetail. GitLab issues will be created for actionable insights.

### Recruitment
We will recruit 5 members of Incident management SIG.

## Procedure
1. General intro
1. Triage workflow part
     1. Intro
     1. Scenario
     1. Task
     1. Post-task interview   
1. Incident issue participant
     1. Initial interview
     1. Introduction
     1. First impressions
     1. Walkthrough
     1. Closing interview
1. Session wrap-up
     
